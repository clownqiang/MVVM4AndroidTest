package com.example.MVVMTest;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import gueei.binding.Binder;
import gueei.binding.Command;
import gueei.binding.observables.StringObservable;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    public StringObservable Greeting = new StringObservable("Hello World!!");
    public Command ChangeGreeting = new Command() {
        @Override
        public void Invoke(View view, Object... objects) {
            Greeting.set("Change Greeting!!!");
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = Binder.bindView(this, Binder.inflateView(this,R.layout.main,null,false),this);
        setContentView(view);
    }
}
