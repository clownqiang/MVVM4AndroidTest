package com.example.MVVMTest;

import android.app.Application;
import gueei.binding.Binder;

/**
 * Created by clownqiang on 14-7-14.
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Binder.init(this);
    }
}
